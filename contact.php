<?php

$to="olekiwanski@gmail.com";
$sub = "Nowy kontakt";

$error_start = "<p class='alert-error'>";
$error_end = "</p>";
$valid_form = true;
$redirect = "success.php";

$form_fields = array("name", "phone", "email", "message");
$required = array("name", "phone", "email");

foreach ($required as $require) {
	$error[$require] = '';
}

if (isset($_POST['submit'])) {

	/* Pobieranie danych */
	foreach ($form_fields as $field) {
		$form[$field] = htmlspecialchars($_POST[$field]);
	}
	
	/* Walidacja */

	if (empty($form['name'])) {
		$error['name'] = $error_start . "Wypełnij wymagane pole" . $error_end;
		$valid_form = FALSE;
	}
	if (empty($form['phone'])) {
		$error['phone'] = $error_start . "Wypełnij wymagane pole" . $error_end;
		$valid_form = FALSE;
	}
	if (empty($form['email'])) {
		$error['email'] = $error_start . "Wypełnij wymagane pole" . $error_end;
		$valid_form = FALSE;
	}
	
	if (empty($error['email']) && !filter_var($form['email'], FILTER_VALIDATE_EMAIL)) {
		$error['email'] = $error_start . "Wprowadź prawidłowy email" . $error_end;
		$valid_form = FALSE;
	}

	if (empty($error['phone']) && !preg_match('^[0-9]{9}$^',$form['phone'])) {
		$error['phone'] = $error_start . "Wprowadź prawidłowy numer telefonu" . $error_end;
		$valid_form = FALSE;
	}

	if ($valid_form == TRUE) {
		
		$message = "Imię:".$form['name']."\n";
		$message .= "Telefon".$form['phone']."\n";
		$message .= "Email".$form['email']."\n";
		$message .= "Wiadomość".$form['message']."\n";
		
		$headers="FROM: olekiwanski@gmail.com <olekiwanski@gmail.com> \r\n";
		$headers.="X-Sender: <olekiwanski@gmail.com>";
		
		mail($to, $sub, $message,$headers);
		
		header("Location: " . $redirect);
	} else {
		include ('form.php');
	}

} else {

	foreach ($form_fields as $field) {
		$form[$field] = '';
	}

	include ('form.php');

}
